package homework7

fun main() {

    val user = createUser("Joe", "qwerty1234", "qwerty1234")
    println(user.login)

    val nullUser = createUserWithNullSafety("Joe", null, "qwerty1234")
    println(nullUser.login)
    println(nullUser.password)

    try {
        val loginException = createUser("HereAreTwenty1Symbols", "qwerty1234", "qwerty1234")
//        val passwordLengthException = createUser("Joe", "qwerty", "qwerty")
//        val passwordMatchException = createUser("Joe", "qwerty1234", "qwerty12345")
    } catch (e: WrongLoginException) {
        println("Caught WrongLoginException")
    } catch (e: WrongPasswordException) {
        println("Caught WrongPasswordException")
    } catch (e: Exception) {
        println("Something went wrong...")
    }
}

// implementation without null safety

fun createUser(login: String, password: String, repeatPassword: String): User {
    if (login.length !in 1..20) {
        throw WrongLoginException("Login has incorrect length - ${login.length}")
    }
    if (password.length < 10) {
        throw WrongPasswordException("Password has incorrect length - ${password.length}")
    }
    if (password != repeatPassword) {
        throw WrongPasswordException("Passwords $password and $repeatPassword do not match")
    }
    return User(login, password)
}

// implementation with null safety

fun createUserWithNullSafety(login: String?, password: String?, repeatPassword: String?): User {
    if (login == null || password == null || repeatPassword == null) {
        return User("NullUser", "NullPassword")
    } else {
        if (login.length !in 1..20) {
            throw WrongLoginException("Login has incorrect length - ${login.length}")
        }
        if (password.length < 10) {
            throw WrongPasswordException("Password has incorrect length - ${password.length}")
        }
        if (password != repeatPassword) {
            throw WrongPasswordException("Passwords $password and $repeatPassword do not match")
        }
        return User(login, password)
    }
}

class WrongLoginException(message: String): Exception(message)

class WrongPasswordException(message: String): Exception(message)

class User(val login: String, val password: String)
