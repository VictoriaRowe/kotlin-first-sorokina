package homework6

abstract class Animal {
    abstract var name: String
    abstract var height: Double
    abstract var weight: Double
    abstract var foodPreferences: Array<String>

    var satiety: Int = 0
        private set

    fun eat(food: String) {
        println("$name is eating $food")
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Lion(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("zebra", "buffalo", "antelope")
}

class Tiger(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("deer", "antelope", "pig")
}

class Hippo(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("grass", "leaves", "fruit")
}

class Wolf(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("deer", "rabbits", "elk")
}

class Giraffe(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("leaves", "grass", "seeds")
}

class Elephant(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("tree bark", "fruit", "leaves")
}

class Chimp(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("fruit", "roots", "nuts")
}

class Gorilla(
    override var name: String,
    override var height: Double,
    override var weight: Double
): Animal() {
    override var foodPreferences: Array<String> = arrayOf("stems", "leaves", "bamboo shoots")
}
