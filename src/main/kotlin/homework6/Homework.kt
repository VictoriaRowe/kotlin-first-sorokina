package homework6

fun feedAllAnimals(animals: Array<Animal>, food: Array<String>) {
    if (food.size < animals.size) {
        println("Not enough food for all animals!")
    } else {
        for (i in animals.indices) {
            animals[i].eat(food[i])
        }
    }
}

fun main() {
    var lion = Lion("Simba", 1.0, 200.0)
    var tiger = Tiger("Rajah", 0.9, 190.9)
    var hippo = Hippo("Gloria", 1.3, 1320.3)
    var wolf = Wolf("Fenrir", 0.75, 45.7)
    var giraffe = Giraffe("Melman", 5.1, 1500.1)
    var elephant = Elephant("Dumbo", 3.8, 3401.8)
    var chimp = Chimp("Abu", 1.25, 55.2)
    var gorilla = Gorilla("2-D", 1.4, 176.4)

    // checking that an animal is eating correctly
    println(lion.satiety)
    lion.eat("zebra")
    println(lion.satiety)
    lion.eat("deer")
    println(lion.satiety)

    // feeding all animals
    val animals = arrayOf(lion, tiger, hippo, wolf, giraffe, elephant, chimp, gorilla)
    val food = arrayOf("zebra", "deer", "grass", "rabbits", "leaves", "tree bark", "fruit", "stems")
    feedAllAnimals(animals, food)
}
