package tictactoe

import java.lang.NumberFormatException

class Player(val name: String, val move: Moves) {
    fun makeMove(field: Field) {
        println("Игрок $name делает ход. Введите координаты поля:")
        try {
            val coordinates = readln().split(" ").map { it.toInt() }
            if (coordinates.size != 2 || !coordinates.all { it in 1..3  }) {
                println("Таких координат не существует. Координаты должны быть двумя числами от 1 до 3.")
                println("Попробуйте еще раз.")
                makeMove(field)
            } else {
                try {
                    field.updateState(this.move, Pair(coordinates[0] - 1, coordinates[1] - 1))
                } catch (e: FieldTakenException) {
                    println(e.message)
                    makeMove(field)
                }
            }
        } catch (e: NumberFormatException) {
            println("Координаты не целые числа.")
            println("Введите координаты поля в одну строчку в формате N M, где N и M - целые числа от 1 до 3.")
            makeMove(field)
        }
    }
}