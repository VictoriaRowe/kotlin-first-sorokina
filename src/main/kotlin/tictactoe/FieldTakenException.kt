package tictactoe

class FieldTakenException(message: String): IllegalStateException(message)
