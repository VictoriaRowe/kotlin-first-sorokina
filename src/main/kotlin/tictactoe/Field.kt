package tictactoe

class Field {
    private val state: Array<Array<String?>> = arrayOf(
        arrayOf(null, null, null),
        arrayOf(null, null, null),
        arrayOf(null, null, null)
    )

    fun printState() {
        val printedField = List(3) { i -> state[i].map { it ?: "*" }}
        for (row in printedField) {
            println(row.joinToString(""))
        }
    }

    @Throws(FieldTakenException::class)
    fun updateState(newValue: Moves, indices: Pair<Int, Int>) {
        if (state[indices.first][indices.second] != null) {
            throw FieldTakenException("Клетка ${indices.first + 1} ${indices.second + 1} уже занята.")
        } else {
            state[indices.first][indices.second] = newValue.symbol
        }
    }

    fun hasWinCombo(move: Moves): Boolean {
        var isWin = false
        for (row in state) {
            if (row.all{ it == move.symbol }) isWin = true
        }
        val columns = arrayOf(
            arrayOf(state[0][0], state[1][0], state[2][0]),
            arrayOf(state[0][1], state[1][1], state[2][1]),
            arrayOf(state[0][2], state[1][2], state[2][2])
        )
        for (column in columns) {
            if (column.all{ it == move.symbol }) isWin = true
        }
        val diagonals = arrayOf(
            arrayOf(state[0][0], state[1][1], state[2][2]),
            arrayOf(state[0][2], state[1][1], state[2][0])
        )
        for (diagonal in diagonals) {
            if (diagonal.all{ it == move.symbol }) isWin = true
        }
        return isWin
    }

    fun isFull(): Boolean = this.state.all { it.all { field -> field != null} }

}