package tictactoe

enum class Moves(val symbol: String) {
    X("x"),
    O("o")
}