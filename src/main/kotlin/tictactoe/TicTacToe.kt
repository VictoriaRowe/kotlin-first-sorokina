package tictactoe

fun main() {
    println("Добро пожаловать в консольную игру крестики-нолики!")
    println("2 игрока должны ходить по очереди, вводя координаты своего хода")
    println("в одну строку в формате N M (например, 1 1 для хода в левой верхней клетке).")

    println("Первый игрок, введите имя:")
    val firstPlayerName = readln()
    println("Второй игрок, введите имя:")
    val secondPlayerName = readln()

    playTicTacToe(firstPlayerName, secondPlayerName)
}

fun playTicTacToe(name1: String, name2: String) {
    val player1 = Player(name1, Moves.X)
    val player2 = Player(name2, Moves.O)
    val field = Field()

    var currentPlayer = player1
    field.printState()
    while(true) {
        currentPlayer.makeMove(field)
        field.printState()
        if (field.hasWinCombo(currentPlayer.move)) {
            println("Игра окончена! Победил игрок ${currentPlayer.name}.")
            break
        }
        if (field.isFull()) {
            println("Игра окончена! Победила дружба.")
            break
        }
        currentPlayer = if (currentPlayer == player1) player2 else player1
    }
}
