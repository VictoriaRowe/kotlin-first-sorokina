package homework5

import kotlin.math.pow

fun main() {
    println("Enter a number: ")

    try {
        val digit = readln().toInt()
        println(reverseNumber(digit))
        println(reverseNumber2(digit))
    } catch (e: NumberFormatException) {
        println("Alas, it's not a number.")
    }
}

// способ 1, простой и дурацкий
fun reverseNumber(num: Int): Int {
    return if (num < 0) {
        -(num * -1).toString().reversed().toInt()
    } else {
        num.toString().reversed().toInt()
    }
}

// способ 2, через остатки от деления
fun reverseNumber2(num: Int): Int {
    val base = if (num >= 0) num.toString().length else num.toString().length - 1
    var currentNum = num
    var newNum = 0
    for (i in base downTo 1) {
        newNum += ((currentNum % 10) * (10.0).pow(i - 1)).toInt()
        currentNum /= 10
    }
    return newNum
}
