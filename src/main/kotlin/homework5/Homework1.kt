package homework5

class Lion(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("zebra", "buffalo", "antelope")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Tiger(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("deer", "antelope", "pig")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Hippo(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("grass", "leaves", "fruit")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Wolf(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("deer", "rabbits", "elk")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Giraffe(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("leaves", "grass", "seeds")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Elephant(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("tree bark", "fruit", "leaves")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Chimp(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("fruit", "roots", "nuts")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

class Gorilla(
    private var name: String,
    private var height: Double,
    private var weight: Double
) {
    private var foodPreferences: Array<String> = arrayOf("stems", "leaves", "bamboo shoots")
    var satiety: Int = 0
        private set

    fun eat(food: String) {
        if (food in foodPreferences) {
            satiety++
        }
    }
}

fun main() {
    var lion = Lion("Simba", 1.0, 200.0)
    var tiger = Tiger("Rajah", 0.9, 190.9)
    var hippo = Hippo("Gloria", 1.3, 1320.3)
    var wolf = Wolf("Fenrir", 0.75, 45.7)
    var giraffe = Giraffe("Melman", 5.1, 1500.1)
    var elephant = Elephant("Dumbo", 3.8, 3401.8)
    var chimp = Chimp("Abu", 1.25, 55.2)
    var gorilla = Gorilla("2-D", 1.4, 176.4)

    lion.eat("zebra")
    println(lion.satiety)
    lion.eat("deer")
    println(lion.satiety)
}
