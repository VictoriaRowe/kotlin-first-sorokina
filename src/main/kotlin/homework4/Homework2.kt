package homework4

fun Double.round(decimals: Int = 2): Double = "%.${decimals}f".format(this).toDouble()

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5, 4)
    var aStudents = 0
    var bStudents = 0
    var cStudents = 0
    var dStudents =  0

    for (mark in marks) {
        when (mark) {
            5 -> aStudents++
            4 -> bStudents++
            3 -> cStudents++
            2 -> dStudents++
        }
    }

    println("Отличников - ${(aStudents.toDouble() / marks.size * 100).round(1)}%")
    println("Хорошистов - ${(bStudents.toDouble() / marks.size * 100).round(1)}%")
    println("Троечников - ${(cStudents.toDouble() / marks.size * 100).round(1)}%")
    println("Двоечников - ${(dStudents.toDouble() / marks.size * 100).round(1)}%")
}