package homework4

fun main() {
    val treasureCoins = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var evenCoins = 0
    var joeMoney = 0
    var oddCoins = 0
    var crewMoney = 0
    for (coin in treasureCoins) {
        if (coin % 2 == 0) {
            evenCoins++
            joeMoney += coin
        } else {
            oddCoins++
            crewMoney += coin
        }
    }
    println("Captain Joe will get $evenCoins coins which is $joeMoney monetary units")
    println("Joe's crew will get $oddCoins coins which is $crewMoney monetary units")
}
