package seabattle

fun main() {
    printWelcomeMessage()

    println("Первый игрок, введите имя:")
    val firstPlayerName = readln()
    println("Второй игрок, введите имя:")
    val secondPlayerName = readln()

    val player1 = Player(firstPlayerName)
    val player2 = Player(secondPlayerName)

    playSeaBattle(player1, player2)
}

fun playSeaBattle(player1: Player, player2: Player) {

    println("${player1.name} размещает корабли! ${player2.name}, отвернитесь!")
    player1.placeOwnShips()
    player1.field.printShipsState()

    println("${player2.name} размещает корабли! ${player1.name}, отвернитесь!")
    player2.placeOwnShips()
    player2.field.printShipsState()

    println("${player1.name} и ${player2.name} разместили корабли, игра начинается!")

    var currentPlayer = player1
    var idlePlayer = player2
    while(true) {
        println()
        println("Ходит игрок ${currentPlayer.name}.")
        println("Поле игрока ${idlePlayer.name}:")
        idlePlayer.field.printHitsState()
        val hasMissed = currentPlayer.makeMove(idlePlayer.field)
        if (hasMissed) {
            currentPlayer = if (currentPlayer == player1) player2 else player1
            idlePlayer = if (idlePlayer == player1) player2 else player1
        }
        if (idlePlayer.field.shipsKilled == Field.SHIPS_NUM) {
            println("Игра окончена! Победил игрок ${currentPlayer.name}.")
            break
        }
    }
}

fun printWelcomeMessage() {
    println("""Добро пожаловать в консольную игру Морской Бой!
        |2 игрока должны разместить корабли на своем поле 4х4 и ходить по очереди,
        |вводя координаты своего выстрела в одну строку в формате N M
        |(например, 1 1 для выстрела в левой верхней клетке,
        |1 4 для выстрела в правой верхней клетке).
        |
    """.trimMargin())
}