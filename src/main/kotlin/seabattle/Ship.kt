package seabattle

sealed class Ship(val decks: Int) {
    abstract val symbol: String

    val maxHP = decks

    var currentHP = maxHP
        private set

    fun damage() = currentHP--
}

class OneDeckShip(): Ship(1) {
    override val symbol = decks.toString()
}

class TwoDeckShip(val direction: ShipDirection): Ship(2) {
    override val symbol = decks.toString()
}