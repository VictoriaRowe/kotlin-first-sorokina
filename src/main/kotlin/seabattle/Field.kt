package seabattle

import seabattle.exceptions.FieldTakenException
import seabattle.exceptions.IncorrectCoordinatesException
import seabattle.exceptions.IncorrectDirectionException
import java.lang.IllegalStateException
import java.lang.IndexOutOfBoundsException
import java.lang.NumberFormatException

class Field {
    var shipsKilled = 0

    private val shipsState: Array<Array<Ship?>> = arrayOf(
        arrayOf(null, null, null, null),
        arrayOf(null, null, null, null),
        arrayOf(null, null, null, null),
        arrayOf(null, null, null, null)
    )

    fun printShipsState() {
        val printedField = List(4) { i -> shipsState[i].map { it?.symbol ?: "_" } }
        for (row in printedField) {
            println(row.joinToString(""))
        }
    }

    private val hitsState: Array<Array<String?>> = arrayOf(
        arrayOf(null, null, null, null),
        arrayOf(null, null, null, null),
        arrayOf(null, null, null, null),
        arrayOf(null, null, null, null)
    )

    fun printHitsState() {
        val printedField = List(4) { i -> hitsState[i].map { it ?: "_" } }
        for (row in printedField) {
            println(row.joinToString(""))
        }
    }

    @Throws(FieldTakenException::class, IllegalStateException::class, IndexOutOfBoundsException::class)
    fun placeShip(ship: Ship, coordinates: Coordinates) {
        val x = coordinates.x
        val y = coordinates.y
        if (shipsState[x][y] != null) {
            throw FieldTakenException("Клетка ${x + 1} ${y + 1} уже занята.")
        }
        when (ship) {
            is OneDeckShip -> {
                if (hasAdjacentShips(coordinates)) {
                    throw IllegalStateException("Корабль не должен соприкасаться с другими по бокам или диагонали.")
                }
                shipsState[x][y] = ship
            }
            is TwoDeckShip -> {
                when (ship.direction) {
                    ShipDirection.DOWN -> shipsState[x + 1][y] = ship
                    ShipDirection.RIGHT -> shipsState[x][y + 1] = ship
                }
                shipsState[x][y] = ship
            }
        }
    }

    private fun hasAdjacentShips(coordinates: Coordinates): Boolean {
        val x = coordinates.x
        val y = coordinates.y
        val adjacentCoordinates = shipsState
            .filterIndexed { index, _ -> index in x - 1..x + 1 }
            .map { it.filterIndexed { index, _ -> index in y - 1..y + 1 } }
        for (row in adjacentCoordinates) {
            for (cell in row) {
                if (cell != null) return true
            }
        }
        return false
    }

    fun getCoordinates(): Coordinates {
        var coordinates: Coordinates
        while (true) {
            try {
                val coordinatesInput = readln().split(" ").map { it.toInt() }
                if (coordinatesInput.size != 2 || !coordinatesInput.all { it in 1..FIELD_SIZE }) {
                    throw IncorrectCoordinatesException("Таких координат не существует: $coordinatesInput")
                }
                coordinates = Coordinates(coordinatesInput[0] - 1, coordinatesInput[1] - 1)
                break
            } catch (e: IncorrectCoordinatesException) {
                println(e.message)
                println("Координаты должны быть двумя числами от 1 до $FIELD_SIZE. Попробуйте еще раз.")
            } catch (e: NumberFormatException) {
                println("Координаты не целые числа.")
                println("Введите координаты в одну строчку в формате N M, где N и M - целые числа от 1 до $FIELD_SIZE.")
            }
        }
        return coordinates
    }

    private fun getShipDirection(): ShipDirection {
        var direction: ShipDirection
        while (true) {
            try {
                when (val directionInput = readln()) {
                    "1" -> {
                        direction = ShipDirection.DOWN
                        break
                    }

                    "2" -> {
                        direction = ShipDirection.RIGHT
                        break
                    }

                    else -> throw IncorrectDirectionException("Неверное направление: $directionInput")
                }
            } catch (e: IncorrectDirectionException) {
                println(e.message)
                println("Введите цифру для выбора направления: 1 - вниз, 2 - вправо.")
            }
        }
        return direction
    }

    fun getCoordinatesAndPlaceShip(isMultiDeck: Boolean = false) {
        while (true) {
            try {
                val shipCoordinates = getCoordinates()
                val ship: Ship = if (isMultiDeck) {
                    println("Введите направление 2-палубного корабля: 1 - вниз, 2 - вправо:")
                    val shipDirection = getShipDirection()
                    TwoDeckShip(shipDirection)
                } else {
                    OneDeckShip()
                }
                placeShip(ship, shipCoordinates)
                break
            } catch (e: FieldTakenException) {
                println(e.message)
                println("Введите координаты заново:")
            } catch (e: IllegalStateException) {
                println(e.message)
                println("Введите координаты заново:")
            } catch (e: IndexOutOfBoundsException) {
                println("Корабль разместить не получится.")
                println("Введите координаты и направление так, чтобы корабль не выходил за пределы поля.")
                println("Введите координаты заново:")
            }
        }
    }

    @Throws(FieldTakenException::class)
    fun takeDamage(coordinates: Coordinates): Boolean {
        var isMiss = false
        val x = coordinates.x
        val y = coordinates.y
        if (hitsState[x][y] == WATER_HIT || hitsState[x][y] == SHIP_HIT) {
            throw FieldTakenException("Вы уже стреляли в клетку ${x + 1} ${y + 1}.")
        }
        when (shipsState[x][y]) {
            null -> {
                hitsState[x][y] = WATER_HIT
                isMiss = true
                println("Мимо!")
            }
            is OneDeckShip -> {
                hitsState[x][y] = SHIP_HIT
                shipsKilled += 1
                println("Корабль убит!")
            }
            is TwoDeckShip -> {
                val currentHP = shipsState[x][y]?.currentHP ?: 0
                val maxHP = shipsState[x][y]?.maxHP ?: 0
                val isDamaged = currentHP < maxHP
                if (isDamaged) {
                    hitsState[x][y] = SHIP_HIT
                    shipsKilled += 1
                    println("Корабль убит!")
                } else {
                    hitsState[x][y] = SHIP_HIT
                    shipsState[x][y]?.damage()
                    println("Корабль ранен!")
                }
            }
        }
        return isMiss
    }

    companion object {
        const val WATER_HIT = "*"
        const val SHIP_HIT = "X"
        const val FIELD_SIZE = 4
        const val SHIPS_NUM = 3
    }
}
