package seabattle.exceptions

class IncorrectDirectionException(message: String): IllegalStateException(message)