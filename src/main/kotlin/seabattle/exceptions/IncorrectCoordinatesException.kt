package seabattle.exceptions

class IncorrectCoordinatesException(message: String): IllegalStateException(message)