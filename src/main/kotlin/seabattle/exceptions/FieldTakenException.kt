package seabattle.exceptions

class FieldTakenException(message: String): IllegalStateException(message)