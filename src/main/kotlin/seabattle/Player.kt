package seabattle

import seabattle.exceptions.FieldTakenException

class Player(val name: String) {

    val field = Field()

    fun placeOwnShips() {
        println("Введите координаты первой палубы 2-палубного корабля:")
        field.getCoordinatesAndPlaceShip(isMultiDeck = true)

        println("Введите координаты первого 1-палубного корабля:")
        field.getCoordinatesAndPlaceShip()

        println("Введите координаты второго 1-палубного корабля:")
        field.getCoordinatesAndPlaceShip()
    }

    fun makeMove(field: Field): Boolean {
        var hasMissed: Boolean
        println("Введите координаты удара:")
        while (true) {
            val coordinates = field.getCoordinates()
            try {
                hasMissed = field.takeDamage(coordinates)
                break
            } catch(e: FieldTakenException) {
                println(e.message)
                println("Введите координаты заново:")
            }
        }
        return hasMissed
    }
}