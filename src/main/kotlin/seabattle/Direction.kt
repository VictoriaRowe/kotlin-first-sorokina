package seabattle

enum class ShipDirection {
    DOWN, RIGHT
}