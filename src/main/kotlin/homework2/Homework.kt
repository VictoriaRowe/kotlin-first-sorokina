fun main() {
    val lemonadeMl: Int = 18500
    val pinaColadaMl: Short = 200
    val whiskeyMl: Byte = 50
    val freshDrops: Long = 3000000000
    val colaLiters: Float = 0.5F
    val aleLiters: Double = 0.666666667
    val special: String = "Что-то авторское!"

    println("Заказ - ‘$lemonadeMl мл лимонада’ готов!")
    println("Заказ - ‘$pinaColadaMl мл пина колады’ готов!")
    println("Заказ - ‘$whiskeyMl мл виски’ готов!")
    println("Заказ - ‘$freshDrops капель фреша’ готов!")
    println("Заказ - ‘$colaLiters литра колы’ готов!")
    println("Заказ - ‘$aleLiters литра эля’ готов!")
    println("Заказ - ‘$special’ готов!")
}
