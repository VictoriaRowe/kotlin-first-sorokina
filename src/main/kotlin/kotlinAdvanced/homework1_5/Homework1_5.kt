package kotlinAdvanced.homework1_5

fun main() {
    fun configureUser(
        name: String,
        surname: String,
        middleName: String? = null,
        gender: String,
        birthdate: String,
        age: Int,
        inn: String? = null,
        snils: String? = null,
    ) {
        println("Данные пользователя:")
        print("name = $name, ")
        print("surname = $surname, ")
        if (middleName != null) print("patronymic = $middleName, ")
        print("gender = $gender, ")
        print("dateOfBirth = $birthdate, ")
        print("age = $age")
        if (inn != null)  print(", inn = $inn")
        if (snils != null)  print(", snils = $snils")
        println()
    }

    configureUser(
        "Виктория",
        "Сорокина",
        "Андреевна",
        "F",
        "1994-10-05",
        29
    )
    println()
    configureUser(
        "Максим",
        "Каммерер",
        "Андреевич",
        "M",
        "2137-06-06",
        20,
        "661212121212",
        "66-123-123-123"
    )
    println()
    configureUser(
        inn = "661212121213",
        snils = "66-123-123-124",
        age = 22,
        birthdate = "2135-10-10",
        surname = "Гаал",
        name = "Гай",
        gender = "M"
    )
    println()
    configureUser(
        surname = "Гаал",
        name = "Рада",
        birthdate = "2134-01-05",
        age = 23,
        gender = "F",
        inn = "661212121218"
    )
}