package kotlinAdvanced.homework2_2

fun Double.round(decimals: Int = 2): Double = "%.${decimals}f".format(this).toDouble()

fun main() {
    fun blackBox(list: List<Double?>): Double {
        val result = list
            .asSequence()
            .filterNotNull()
            .map {
                if (it.toInt() % 2 == 0) it * it
                else it / 2
            }
            .filter { it < 25 }
            .sortedDescending()
            .take(10)
            .sum()
        return result
    }

    val list1 = listOf(13.31, 3.98, 12.0, 2.99, 9.0)
    val list2 = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)
    val list3 = listOf(13.31, 3.98, 12.0, 2.99, 9.0, 1.12, 2.34, 4.45, 5.67, 7.65, 9.1)

    println(blackBox(list1).round(2))
    println(blackBox(list2).round(2))
    println(blackBox(list3).round(2))
}