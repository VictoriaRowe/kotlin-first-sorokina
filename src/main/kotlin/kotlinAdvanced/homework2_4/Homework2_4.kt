package kotlinAdvanced.homework2_4

abstract class Pet

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

open class Cat: Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am ${this::class.simpleName}, and I am running")
    }

    override fun swim() {
        println("I am ${this::class.simpleName}, and I am swimming")
    }
}

class Tiger: Cat()

class Lion: Cat()

open class Fish: Pet(), Swimmable {
    override fun swim() {
        println("I am ${this::class.simpleName}, and I am swimming")
    }

}

class Salmon: Fish()

class Tuna: Fish()

fun useRunSkill(obj: Runnable) {
    obj.run()
}

fun useSwimSkill(obj: Swimmable) {
    obj.swim()
}

fun <T> useSwimAndRunSkill(obj: T) where T: Runnable, T: Swimmable {
    obj.swim()
    obj.run()
}

fun main() {
    val tiger = Tiger()
    useRunSkill(tiger)
    useSwimSkill(tiger)
    useSwimAndRunSkill(tiger)
    println()

    val lion = Lion()
    useRunSkill(lion)
    useSwimSkill(lion)
    useSwimAndRunSkill(lion)
    println()

    val salmon = Salmon()
    useSwimSkill(salmon)
    println()

    val tuna = Tuna()
    useSwimSkill(tuna)
}