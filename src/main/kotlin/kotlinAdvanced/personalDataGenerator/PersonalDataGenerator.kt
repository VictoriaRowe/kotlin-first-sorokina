package kotlinAdvanced.personalDataGenerator

import kotlinAdvanced.personalDataGenerator.utils.JsonFileGenerator
import kotlinAdvanced.personalDataGenerator.utils.PdfFileGenerator
import kotlinAdvanced.personalDataGenerator.utils.PersonsGenerator
import java.lang.NumberFormatException

fun main() {
    println("Введите число записей для генерации от 1 до 30:")
    val count = getNumberOfPersonsToGenerate()

    println("Генерируем записей: $count...")
    val persons = PersonsGenerator().generate(count)
    JsonFileGenerator(persons).generate()
    PdfFileGenerator(persons).generate()
}

fun getNumberOfPersonsToGenerate(): Int {
    var n: Int
    while (true) {
        try {
            n = readln().toInt()
            if (n !in 1..30) {
                println("Введите, пожалуйста, число от 1 до 30.")
                continue
            }
            break
        } catch (e: NumberFormatException) {
            println("Введите, пожалуйста, число.")
        }
    }
    return n
}
