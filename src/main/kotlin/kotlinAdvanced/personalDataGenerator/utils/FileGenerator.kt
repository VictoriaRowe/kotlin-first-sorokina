package kotlinAdvanced.personalDataGenerator.utils

import java.io.File

interface FileGenerator {
    val outputDirName: String
        get() = "output"

    fun createOutputDir() {
        val outputDir = File(outputDirName)
        if (!outputDir.exists()) {
            outputDir.mkdir()
        }
    }
    fun generate()
}