package kotlinAdvanced.personalDataGenerator.utils

enum class Gender(val naming: String) {
    FEMALE("ЖЕН"),
    MALE("МУЖ")
}