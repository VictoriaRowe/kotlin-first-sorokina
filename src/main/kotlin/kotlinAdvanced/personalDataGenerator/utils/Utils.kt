package kotlinAdvanced.personalDataGenerator.utils

import kotlinAdvanced.personalDataGenerator.dataClasses.Address
import kotlinAdvanced.personalDataGenerator.dataClasses.FullName
import java.time.LocalDate
import java.time.Period
import kotlin.random.Random

fun generateGender(): Gender {
    return Gender.entries.random()
}
fun generateNames(gender: Gender): FullName {
    val firstName: String
    val lastName: String
    val middleName: String
    when(gender) {
        Gender.FEMALE -> {
            firstName = femaleFirstNames.random()
            lastName = femaleLastNames.random()
            middleName = femaleMiddleNames.random()
        }
        Gender.MALE -> {
            firstName = maleFirstNames.random()
            lastName = maleLastNames.random()
            middleName = maleMiddleNames.random()
        }
    }
    return FullName(firstName, lastName, middleName)
}

fun generateAge(): Int {
    return Random.nextInt(18, 100)
}

fun generateBirthdate(yearsAgo: Int): String {
    val now = LocalDate.now()
    val birthdate = now.minus(Period.of(yearsAgo, Random.nextInt(1, 13), Random.nextInt(1, 32)))
    val day = if (birthdate.dayOfMonth > 9) "${birthdate.dayOfMonth}" else "0${birthdate.dayOfMonth}"
    val month = if (birthdate.monthValue > 9) "${birthdate.monthValue}" else "0${birthdate.monthValue}"
    return "$day-$month-${birthdate.year}"
}

fun generateBirthplace(): String {
    return cities.random()
}

fun generateInn(): String {
    var inn = ""
    repeat(10) {
        inn += Random.nextInt(1,10)
    }
    val coefficients1 = arrayOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
    var controlSum1 = 0
    for (i in coefficients1.indices) {
        controlSum1 += inn[i].digitToInt() * coefficients1[i]
    }
    controlSum1 = controlSum1 % 11 % 10
    inn += controlSum1
    val coefficients2 = arrayOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
    var controlSum2 = 0
    for (i in coefficients2.indices) {
        controlSum2 += inn[i].digitToInt() * coefficients2[i]
    }
    controlSum2 = controlSum2 % 11 % 10
    inn += controlSum2
    return inn
}

fun generateAddress(): Address {
    return Address(
        index = Random.nextInt(100000, 999999).toString(),
        country = "Россия",
        area = areas.random(),
        city = cities.random(),
        street = streets.random(),
        house = Random.nextInt(1, 101).toString(),
        flat = Random.nextInt(1, 501).toString()
    )
}
