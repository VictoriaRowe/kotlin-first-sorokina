package kotlinAdvanced.personalDataGenerator.utils

import kotlinAdvanced.personalDataGenerator.dataClasses.Person


class PersonsGenerator() {

    fun generate(numberOfPersons: Int): MutableList<Person> {
        val persons: MutableList<Person> = mutableListOf()
        repeat(numberOfPersons) {
            val gender = generateGender()
            val (firstName, lastName, middleName) = generateNames(gender)
            val age = generateAge()
            val person = Person(
                firstName = firstName,
                lastName = lastName,
                middleName = middleName,
                age = age,
                gender = gender.naming,
                birthdate = generateBirthdate(age),
                birthplace = generateBirthplace(),
                inn = generateInn(),
                address = generateAddress()
            )
            persons.add(person)
        }
        return persons
    }
}
