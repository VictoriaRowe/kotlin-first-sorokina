package kotlinAdvanced.personalDataGenerator.dataClasses

import kotlinx.serialization.Serializable

@Serializable
data class Address(
    val index: String,
    val country: String,
    val area: String,
    val city: String,
    val street: String,
    val house: String,
    val flat: String
)