package kotlinAdvanced.personalDataGenerator.dataClasses

import kotlinx.serialization.Serializable

@Serializable
data class Person(
    val firstName: String,
    val lastName: String,
    val middleName:String,
    val age: Int,
    val gender: String,
    val birthdate: String,
    val birthplace: String,
    val inn: String,
    val address: Address
)