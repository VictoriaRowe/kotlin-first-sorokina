package kotlinAdvanced.personalDataGenerator.dataClasses

data class FullName(val first: String, val last: String, val middle: String)