package kotlinAdvanced.homework2_8

class InnValidationException(message: String) : Exception(message)

fun String.hasOnlyDigits(): Boolean = this.contains("[^0-9]".toRegex())

val innHasValidLength: (String) -> Boolean = { inn: String -> inn.length !in arrayOf(10, 12) }

val validateInn: (String) -> Boolean = { inn: String ->
    var result = false
    when {
        inn.hasOnlyDigits() -> throw InnValidationException("ИНН может состоять только из цифр")
        innHasValidLength(inn) -> throw InnValidationException(
            "Подано ${inn.length} цифр. ИНН может состоять только из 10 или 12 цифр"
        )
    }
    val calculateControlSum: (Array<Int>) -> Int = { coefficients: Array<Int> ->
        var controlSum = 0
        for (i in coefficients.indices) {
            controlSum += inn[i].digitToInt() * coefficients[i]
        }
        controlSum% 11 % 10
    }
    when (inn.length) {
        10 -> {
            val controlSum = calculateControlSum(arrayOf(2, 4, 10, 3, 5, 9, 4, 6, 8))
            result = controlSum == inn[9].digitToInt()
        }
        12 -> {
            val controlSum1 = calculateControlSum(arrayOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8))
            val controlSum2 = calculateControlSum(arrayOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8))
            result = controlSum1 == inn[10].digitToInt() && controlSum2 == inn[11].digitToInt()
        }
    }
    result
}

fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}

fun main() {
    val testInns = arrayOf(
        "707574944998", // valid - 12 digits
        "485617716726", // valid - 12 digits
        "2787165878", // valid - 10 digits
        "8849697410", // valid - 10 digits
        "707574944999", // not valid
        "7075749448", // not valid
        "707574944", // not valid - 9 digits
        "70757494499", // not valid - 11 digits
        "7075749449995", // not valid - 13 digits
        "70757494499a", // not valid - not digits
        "" // not valid - empty
    )

    for (inn in testInns) {
        try {
            validateInnFunction(inn, validateInn)
        } catch (e: InnValidationException) {
            println(e.message)
        } finally {
            println()
        }
    }
}
