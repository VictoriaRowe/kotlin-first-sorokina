package kotlinAdvanced.homework1_7

fun main() {
    fun acceptVararg(vararg params: String) {
        println("Передано элементов: ${params.size}")
        params.forEach {
            print("$it;")
        }
    }
    val testVararg = arrayOf("12", "34", "qwe", "asd")
    acceptVararg(*testVararg)
    println()
    acceptVararg("1", "kek")
    println()
    acceptVararg()
}