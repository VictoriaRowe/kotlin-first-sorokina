package kotlinAdvanced.homework1_3

fun main() {
    fun MutableList<Int>.allMembersSquared(): MutableList<Int> = this.map { it * it }.toMutableList()
    val testList = mutableListOf(1, 4, 9, 16, 25)
    println(testList.allMembersSquared())
}