package kotlinAdvanced.homework1_13

import java.time.LocalDate

fun Double.round(decimals: Int = 2): Double = "%.${decimals}f".format(this).toDouble()

fun main() {
    val tinkoffBirthday = LocalDate.of(2006, 12, 24)
    fun typeTeller(anything: Any?) {
        when(anything) {
            is String -> println("Я получил String = '$anything', ее длина равна ${anything.length}")
            is Int -> println("Я получил Int = $anything, его квадрат равен ${anything * anything}")
            is Double -> {
                print("Я получил Double = $anything, это число округляется до ")
                val roundedDouble = anything.round(2)
                if (roundedDouble * 100 % 100 == 0.0) println(roundedDouble.toInt())
                else println(roundedDouble)
            }
            is LocalDate -> {
                print("Я получил LocalDate $anything, ")
                if (anything < tinkoffBirthday) {
                    println("она меньше даты основания Tinkoff")
                } else if(anything == tinkoffBirthday) {
                    println("она такая же, как дата основания Tinkoff")
                } else {
                    println("она больше даты основания Tinkoff")
                }
            }
            null -> println("Я получил null")
            else -> println("Мне этот тип неизвестен")
        }
    }

    typeTeller("Привет, Андрей, ну где ты был, ну обними меня скорей!")
    typeTeller(145)
    typeTeller(145.0)
    typeTeller(145.2817812)
    typeTeller(145.879)
    typeTeller(LocalDate.of(1990,1,1))
    typeTeller(LocalDate.of(2006,12,24))
    typeTeller(LocalDate.of(2023,1,1))
    typeTeller(null)
    typeTeller(1f)
}