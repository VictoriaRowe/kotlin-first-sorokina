package kotlinAdvanced.homework1_11

class OuterClass {
    companion object {
        private var counter = 0
        fun counter() {
            counter++
            println("Вызван counter. Количество вызовов = $counter")
        }
    }
}

fun main() {
    OuterClass.counter()
    OuterClass.counter()
    OuterClass.counter()
}
