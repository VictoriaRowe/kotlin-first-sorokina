package kotlinAdvanced.homework1_9

data class Person(
    val name: String,
    val surname: String,
    val middleName:String?,
    val gender: String,
    val birthdate: String,
    val age: Int,
    val inn: String?,
    val snils: String?
)

fun main() {
    fun configureUser(
        name: String,
        surname: String,
        middleName:String? = null,
        gender: String,
        birthdate: String,
        age: Int,
        inn: String? = null,
        snils: String? = null
    ): Person {
        return Person(name, surname, middleName, gender, birthdate, age, inn, snils)
    }

    val vika = configureUser(
        "Виктория",
        "Сорокина",
        "Андреевна",
        "F",
        "1994-10-05",
        29
    )
    println(vika)

    val maksim = configureUser(
        "Максим",
        "Каммерер",
        "Андреевич",
        "M",
        "2137-06-06",
        20,
        "661212121212",
        "66-123-123-123"
    )
    println(maksim)

    val guy = configureUser(
        inn = "661212121213",
        snils = "66-123-123-124",
        age = 22,
        birthdate = "2135-10-10",
        surname = "Гаал",
        name = "Гай",
        gender = "M"
    )
    println(guy)

    val rada = configureUser(
        surname = "Гаал",
        name = "Рада",
        birthdate = "2134-01-05",
        age = 23,
        gender = "F"
    )
    println(rada)
}