package kotlinAdvanced.homework2_7

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiplication = { a: Double, b: Double -> a * b }
val division = { a: Double, b: Double ->
    if (b == 0.0) throw ArithmeticException("Делить на ноль нельзя")
    else a / b
}

fun main() {
    val testData: Array<Triple<String, Double, Double>> = arrayOf(
        Triple("+", 2.0, 3.0),
        Triple("-", 2.0, 13.0),
        Triple("*", 4.0, 10.0),
        Triple("/", 12.0, 6.0),
        Triple("/", 12.0, 0.0),
        Triple("%", 1.0, 1.0)
    )
    for (data in testData) {
        try {
            val (symbol, a, b) = data
            println(calculator(getCalculationMethod(symbol), a, b))
        } catch (e: Exception) {
            println(e.message)
        }
    }
}

fun calculator(lambda : ((Double, Double)-> Double), a: Double, b: Double ): Double {
    return lambda(a,b)
}

fun getCalculationMethod(name: String) :(Double, Double)-> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiplication
        "/" -> division
        else -> throw UnsupportedOperationException("Такая операция не поддерживается")
    }
}
