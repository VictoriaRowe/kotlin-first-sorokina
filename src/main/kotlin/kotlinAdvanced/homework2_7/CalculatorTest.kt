package kotlinAdvanced.homework2_7

import junit.framework.TestCase.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class CalculatorSumTest(
    private val firstOperand: Double,
    private val secondOperand: Double,
    private val expectedValue: Double
) {
    @Test
    fun calculatorWithSumTest() {
        assertEquals("Ошибка при сложении", calculator(sum, firstOperand, secondOperand), expectedValue)
    }

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): List<Array<Double>>  {
            return listOf(
                arrayOf(2.0, 3.0, 5.0),
                arrayOf(2.0, -3.0, -1.0),
                arrayOf(-2.0, -3.0, -5.0),
                arrayOf(0.0, 0.0, 0.0)
            )
        }
    }
}

@RunWith(Parameterized::class)
class CalculatorSubtractionTest(
    private val firstOperand: Double,
    private val secondOperand: Double,
    private val expectedValue: Double
) {
    @Test
    fun calculatorWithSubtractionTest() {
        assertEquals("Ошибка при вычитании", calculator(subtraction, firstOperand, secondOperand), expectedValue)
    }

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): List<Array<Double>>  {
            return listOf(
                arrayOf(2.0, 13.0, -11.0),
                arrayOf(2.0, -13.0, 15.0),
                arrayOf(-2.0, -3.0, 1.0),
                arrayOf(0.0, 0.0, 0.0)
            )
        }
    }
}

@RunWith(Parameterized::class)
class CalculatorMultiplicationTest(
    private val firstOperand: Double,
    private val secondOperand: Double,
    private val expectedValue: Double
) {
    @Test
    fun calculatorWithMultiplicationTest() {
        assertEquals("Ошибка при умножении", calculator(multiplication, firstOperand, secondOperand), expectedValue)
    }

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): List<Array<Double>>  {
            return listOf(
                arrayOf(4.0, 10.0, 40.0),
                arrayOf(-2.0, -3.0, 6.0),
                arrayOf(-2.0, 3.0, -6.0),
                arrayOf(0.0, 0.0, 0.0)
            )
        }
    }
}

@RunWith(Parameterized::class)
class CalculatorDivisionTest(
    private val firstOperand: Double,
    private val secondOperand: Double,
    private val expectedValue: Double
) {
    @Test
    fun calculatorWithDivisionTest() {
        assertEquals("Ошибка при делении", calculator(division, firstOperand, secondOperand), expectedValue)
    }

    @Test
    fun calculatorWithDivisionByZeroTest() {
        assertThrows(ArithmeticException::class.java) { calculator(division, 4.0, 0.0) }
    }
    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): List<Array<Double>>  {
            return listOf(
                arrayOf(6.0, 3.0, 2.0),
                arrayOf(-2.0, -4.0, 0.5),
                arrayOf(-2.0, 4.0, -0.5),
                arrayOf(0.0, 1.0, 0.0)
            )
        }
    }
}

class GetCalculationMethodTest {
    @Test
    fun getSumFunctionTest() {
        assertEquals("Ошибка при получении функции сложения", getCalculationMethod("+"), sum)
    }

    @Test
    fun getSubstractionFunctionTest() {
        assertEquals("Ошибка при получении функции вычитания", getCalculationMethod("-"), subtraction)
    }

    @Test
    fun getMultiplicationFunctionTest() {
        assertEquals("Ошибка при получении функции умножения", getCalculationMethod("*"), multiplication)
    }

    @Test
    fun getDivisionFunctionTest() {
        assertEquals("Ошибка при получении функции деления", getCalculationMethod("/"), division)
    }

    @Test
    fun getUnsupportedFunctionTest() {
        assertThrows(UnsupportedOperationException::class.java) { getCalculationMethod("%") }
    }

}