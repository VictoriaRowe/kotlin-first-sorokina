package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Vegetables: ${countVegetables(userCart)}")
    println("Total price: ${calculateTotalPrice(userCart)}")
}

fun countVegetables(cart: Map<String, Int>): Int {
    var counter = 0
    for (item in cart) {
        if (vegetableSet.contains(item.key)) {
            counter += item.value
        }
    }
    return counter
}

fun calculateTotalPrice(cart: Map<String, Int>): Double {
    var totalPrice = 0.0
    for (item in cart) {
        var price: Double
        if (prices.contains(item.key)) {
            price = (prices[item.key] ?: 0.0) * item.value
            if (discountSet.contains(item.key)) {
                price *= (1.0 - discountValue)
            }
            totalPrice += price
        } else {
            println("There is no price for ${item.key}, it won't be included in total price.")
        }
    }
    return totalPrice
}
