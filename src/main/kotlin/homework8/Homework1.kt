package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    val uniqueList = removeDuplicates(sourceList)
    println(uniqueList)
}

fun removeDuplicates(list: List<Any>): List<Any> = list.toSet().toList()
